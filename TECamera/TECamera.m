//
//  AVCamera.m
//  Untitled
//
//  Created by GDX-Mac on 2011/2/28.
//  Copyright 2011 sio2interactive.com. All rights reserved.
//


#import "TECamera.h"

@implementation TECamera

@synthesize captureSession = _captureSession;
@synthesize captureDevice = _captureDevice;
@synthesize videoCaptureOutput = _videoCaptureOutput;
@synthesize delegate = _delegate;
@synthesize flashMode = _flashMode;
@synthesize focusMode = _focusMode;
@synthesize currentImage = _currentImage;
@synthesize sessionPreset = _sessionPreset;

#pragma mark - Object lifecycle

- (void)initCaptureSession {
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
	AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput 
										  deviceInputWithDevice:captureDevice
										  error:nil];
	self.videoCaptureOutput = [[AVCaptureVideoDataOutput alloc] init];
	self.videoCaptureOutput.alwaysDiscardsLateVideoFrames = YES;
    
	dispatch_queue_t queue;
	queue = dispatch_queue_create("cameraQueue", NULL);
	[self.videoCaptureOutput setSampleBufferDelegate:self queue:queue];
	dispatch_release(queue);
    
	NSString* key = (NSString *)kCVPixelBufferPixelFormatTypeKey; 
	NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA]; 
	NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key]; 
	[self.videoCaptureOutput setVideoSettings:videoSettings];
    
	AVCaptureSession *captureSession = [[AVCaptureSession alloc] init];
    
	[captureSession addInput:captureInput];
	[captureSession addOutput:self.videoCaptureOutput];
    
    _stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
    NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys: AVVideoCodecJPEG, AVVideoCodecKey, nil];
    [_stillImageOutput setOutputSettings:outputSettings];
    [captureSession addOutput:_stillImageOutput];
    
    self.captureDevice = captureDevice;
    self.captureSession = captureSession;
}

- (id)init {
    if ((self = [super init])) {
        [self initCaptureSession];
        self.flashMode = AVCaptureFlashModeAuto;
        self.focusMode = AVCaptureFocusModeContinuousAutoFocus;
        self.sessionPreset = AVCaptureSessionPresetPhoto;
    }
    return self;
}

#pragma mark - Properties

- (void)setFlashMode:(AVCaptureFlashMode)flashMode {
    NSError *error = nil;
    [self.captureDevice lockForConfiguration:&error];
    if ([self.captureDevice isFlashModeSupported:flashMode]) {
        self.captureDevice.flashMode = flashMode;
        _flashMode = flashMode;
    }
    [self.captureDevice unlockForConfiguration];
}

- (void)setFocusMode:(AVCaptureFocusMode)focusMode {
    NSError *error = nil;
    [self.captureDevice lockForConfiguration:&error];
    if ([self.captureDevice isFocusModeSupported:focusMode]) {
        self.captureDevice.focusMode = focusMode;
        _focusMode = focusMode;
    }
    [self.captureDevice unlockForConfiguration];
}

- (void)setSessionPreset:(NSString *)sessionPreset {
    if (![_sessionPreset isEqualToString:sessionPreset]) {
        if ([self.captureSession canSetSessionPreset:sessionPreset]) {
            BOOL restartCaptureSession = self.captureSession.isRunning;
            if (restartCaptureSession) {
                [self.captureSession stopRunning];
            }
            self.captureSession.sessionPreset = sessionPreset;
            if (restartCaptureSession) {
                [self.captureSession startRunning];
            }
            _sessionPreset = [sessionPreset copy];
        }
    }
}

#pragma mark - Initialization

- (void)startCapture {
	[self.captureSession startRunning];
}

- (AVCaptureVideoPreviewLayer *)getPreviewLayerWithSize:(CGSize)size videoGravity:(NSString *)gravity {
    AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.captureSession];
    CGRect rect = CGRectZero;
    rect.size = size;
    previewLayer.frame = rect;
    previewLayer.videoGravity = gravity;
    return previewLayer;
}

- (void)stopCapture {
	[self.captureSession stopRunning];
}

- (void)takePhoto {
    if (self.captureSession.isRunning) {
        AVCaptureConnection *videoConnection = nil;
        for (AVCaptureConnection *connection in _stillImageOutput.connections) {
            for (AVCaptureInputPort *port in [connection inputPorts]) {
                if ([[port mediaType] isEqual:AVMediaTypeVideo]) {
                    videoConnection = connection;
                    break;
                }
            }
            if (videoConnection) { break; }
        }
        if (videoConnection == nil) return;
        [_stillImageOutput captureStillImageAsynchronouslyFromConnection:videoConnection 
                                                       completionHandler: ^(CMSampleBufferRef imageSampleBuffer, NSError *error) {
                                                           if (imageSampleBuffer != NULL) {
                                                               if ([self.delegate respondsToSelector:@selector(cameraDidCapturedImageData:)]) {
                                                                   NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageSampleBuffer];
                                                                   [self.delegate cameraDidCapturedImageData:imageData];
                                                               }
                                                           }
         }];
    }
}

#pragma mark -
#pragma mark AVCaptureSession delegate

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    
    @autoreleasepool {
        static BOOL processing = NO;
        
        if (captureOutput != _stillImageOutput && [self.delegate respondsToSelector:@selector(cameraDidCapturePreviewImage:)]) {
            if (!processing) {
                processing = YES;
                CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer); 
                CVPixelBufferLockBaseAddress(imageBuffer,0); 
                uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer); 
                size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer); 
                size_t width = CVPixelBufferGetWidth(imageBuffer); 
                size_t height = CVPixelBufferGetHeight(imageBuffer);  
                
                CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB(); 
                CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
                CGImageRef imageRef = CGBitmapContextCreateImage(newContext);
                
                CGContextRelease(newContext); 
                CGColorSpaceRelease(colorSpace);
                self.currentImage = [UIImage imageWithCGImage:imageRef
                                                        scale:1.0 
                                                  orientation:UIImageOrientationRight];
                [self.delegate cameraDidCapturePreviewImage:self.currentImage];
                CGImageRelease(imageRef);
                
                CVPixelBufferUnlockBaseAddress(imageBuffer,0);
                processing = NO;
            }
        }
    }
} 

@end