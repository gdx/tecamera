//
//  AVCamera.h
//  Untitled
//
//  Created by GDX-Mac on 2011/2/28.
//  Copyright 2011 sio2interactive.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>

@protocol TECameraDelegate;


@interface TECamera : NSObject <AVCaptureVideoDataOutputSampleBufferDelegate> {
    AVCaptureStillImageOutput *_stillImageOutput;
}

@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, retain) AVCaptureDevice *captureDevice;
@property (nonatomic, retain) AVCaptureVideoDataOutput *videoCaptureOutput;
@property (nonatomic, assign) id <TECameraDelegate> delegate;
@property (nonatomic, assign) AVCaptureFlashMode flashMode;
@property (nonatomic, assign) AVCaptureFocusMode focusMode;
@property (nonatomic, copy) NSString *sessionPreset;
@property (nonatomic, retain) UIImage *currentImage;

- (void)startCapture;
- (void)stopCapture;
- (void)takePhoto;
- (AVCaptureVideoPreviewLayer *)getPreviewLayerWithSize:(CGSize)size videoGravity:(NSString *)gravity;

@end


@protocol TECameraDelegate <NSObject>

@optional
- (void)cameraDidCapturePreviewImage:(UIImage *)image;
- (void)cameraDidCapturedImageData:(NSData *)imageData;

@end